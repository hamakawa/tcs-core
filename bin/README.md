# 概要
#### TCS-COREとは、TCSの研修で作成した全てのシステムに対して、認証機能(ログイン機能)と社員情報を提供をおこなう。
<br>

## ドキュメント
- [提供機能一覧](https://gitlab.com/hamakawa/tcs-core/wikis/1_%E6%8F%90%E4%BE%9B%E6%A9%9F%E8%83%BD%E4%B8%80%E8%A6%A7)
- [パッケージ構成](https://gitlab.com/hamakawa/tcs-core/wikis/2_%E3%83%91%E3%83%83%E3%82%B1%E3%83%BC%E3%82%B8%E6%A7%8B%E6%88%90)
- [必要開発ツール](https://gitlab.com/hamakawa/tcs-core/wikis/3_%E5%BF%85%E8%A6%81%E9%96%8B%E7%99%BA%E3%83%84%E3%83%BC%E3%83%AB)
- [認証の仕組み](https://gitlab.com/hamakawa/tcs-core/wikis/4_%E8%AA%8D%E8%A8%BC%E3%81%AE%E4%BB%95%E7%B5%84%E3%81%BF)


