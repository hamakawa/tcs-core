/**
 * 認証及び社員情報を取得するクラス
 */
class tcsCoreClient {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
	 * 認証を実施します。
	 */
    auth(data) {
    	var url = this.baseUrl;
    	var sendData = this._sendData;
    	return new Promise(function(resolve, reject){
    		sendData('post',url + '/api/auth',data)
    		.then(function(data){
    			$.cookie.json = true;
            	$.cookie("token", data.token, { expires: 1 });
            	$.cookie('authUser',data,{ expires: 1 });
            	resolve(data);
    		}).catch(function (error) {
    			reject(error.responseJSON);
    		});
    	});
    }

    /**
	 * ログアウトを実施します。
	 */
    logout(){
    	this._sendData('get',this.baseUrl + '/api/auth/delete/',null,$.cookie('token'))
    	.catch(function (error) {

		});
    	$.cookie("token","",{expires:-1});
    	$.cookie('authUser',"",{expires:-1});
    	$.cookie.json = false;
    }

    /**
	 * 認証済み利用者を取得する。
	 */
    getAuthUser(){
    	return $.cookie('authUser');
    }

    /**
	 * 認証済みか未認証かを返します。
	 */
    isAuth(){
    	var token = $.cookie('token');
    	if(token != undefined && token != null){
    		return true;
    	}else{
    		return false;
    	}
    }

    /**
	 * 社員情報の一覧を取得する。
	 */
    getEmpioyeeList(){
    	return this._sendData('get',this.baseUrl + '/api/employee',null,$.cookie('token'));
    }

    /**
	 * 指定のIDの社員情報を取得する
	 */
    getEmpioyee(id){
    	return this._sendData('get',this.baseUrl + '/api/employee/'+id,null,$.cookie('token'));
    }

    /**
	 * 通信を実施します。 privateメソッド
	 */
    _sendData(type,url,data,token){
    	return new Promise(function(resolve, reject){

    		var headers={};
    		if(token != undefined && token != null){
    		 headers.Authorization = "token "+$.cookie("token");
    		}

    		var jsonData = null;
    		if(data != undefined && data != null){
    			jsonData = JSON.stringify(data);
    		}

    		console.log()
    	    $.ajax({
    	    	type: type,
    	        url: url,
    	        headers:headers,
    	        dataType: 'json',
    	        contentType: 'application/json',
    	        data: jsonData,
    	        success: function(data) {
    	        	resolve(data);
    	        },
    	        error: function(error) {
    	        	reject(error.responseJSON);
    	        }
    	       });
    	});
    }
}