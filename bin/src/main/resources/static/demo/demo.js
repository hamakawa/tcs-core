$(() => {

	// 認証及び社員情報を取得するクラスの初期化(ベースとなるURLまで指定)
	var client = new tcsCoreClient("http://localhost:8080/tcs/core/");

	/**
	 * ログインボタンが押下された場合の処理
	 */
	$('#login').click(function(){

		var data = {};
		data.account = $("#account").val();
		data.password = $("#password").val();

		// ユーザー認証を実施する。
		client.auth(data)
		.then(function(data){
        	console.log(data);
        	$("#result").text(JSON.stringify(data));
		}).catch(function (error) {
			$("#result").text(JSON.stringify(error));
        	console.log(error.responseJSON);
		});
	});

	/**
	 * ログアウトボタンが押下された場合の処理
	 */
	$('#logout').click(function(){
		client.logout();
	});

	/**
	 * 社員情報一覧取得ボタンが押下された場合の処理
	 */
	$('#empioyeeList').click(function(){
		client.getEmpioyeeList().then(function(data){
			var text="";
			data.forEach(function(empioyee){
				console.log(empioyee);
				text = text+JSON.stringify(empioyee)+"\n";
			});
			$("#result").text(text);
		}).catch(function (error) {
			$("#result").text(JSON.stringify(error));
			console.log(error);
		});
	});


	/**
	 * 社員情報取得ボタンが押下された場合の処理
	 */
	$('#empioyee').click(function(){

		var id = $("#empioyee_id").val();
		client.getEmpioyee(id).then(function(data){
			console.log(data);
			$("#result").text(JSON.stringify(data));
		}).catch(function (error) {
			$("#result").text(JSON.stringify(error));
			console.log(error);
		});
	});


});
