# 社員
CREATE TABLE employees(
 id              INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '社員ID',
 employee_no     VARCHAR(10)  NOT NULL COMMENT '社員番号',
 last_name       VARCHAR(255) NOT NULL COMMENT '姓',
 first_name      VARCHAR(255) NOT NULL COMMENT '名',
 last_name_kana  VARCHAR(255) COMMENT '姓カナ',
 first_name_kana VARCHAR(255) COMMENT '名カナ',
 tel             VARCHAR(255) COMMENT '電話番号',
 mobile_tel      VARCHAR(255) COMMENT '携帯番号',
 mail            VARCHAR(255) COMMENT 'メール',
 account         VARCHAR(255) NOT NULL COMMENT 'アカウント',
 password        VARCHAR(255) NOT NULL COMMENT 'パスワード',
 created_at      DATETIME      NOT NULL COMMENT '作成日時',
 created_user    VARCHAR(500) NOT NULL COMMENT '作成者',
 updated_at      DATETIME      NOT NULL COMMENT '更新日時',
 updated_user    VARCHAR(500) NOT NULL COMMENT '更新者',
 is_deleted      BOOLEAN      NOT NULL COMMENT '削除フラグ',
 PRIMARY KEY(id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;