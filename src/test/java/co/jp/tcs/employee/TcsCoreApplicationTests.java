package co.jp.tcs.employee;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest
public class TcsCoreApplicationTests {

	@Test
	public void contextLoads() {
	}

}
