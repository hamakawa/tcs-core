/**
 * 認証及び社員情報を取得するクラス
 */
class tcsCoreClient {
    constructor(baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
	 * 認証を実施します。
	 */
    auth(data) {
    	var url = this.baseUrl;
    	var sendData = this._sendData;
    	return new Promise(function(resolve, reject){
    		sendData('post',url + '/api/auth',data)
    		.then(function(data){
    			localStorage.setItem("token", data.token);
    			localStorage.setItem("createTime", new Date().getTime());
    			localStorage.setItem("authUser", JSON.stringify(data));
            	resolve(data);
    		}).catch(function (error) {
    			console.log(error);
    			reject(error.responseJSON);
    		});
    	});
    }

    /**
	 * ログアウトを実施します。
	 */
    logout(){
    	this._sendData('get',this.baseUrl + '/api/auth/delete/',null,localStorage.getItem('token'))
    	.catch(function (error) {

		});
    	localStorage.clear();
    }

    /**
	 * 認証済み利用者を取得する。
	 */
    getAuthUser(){
    	if(this._expirationTime(localStorage.getItem('createTime'))){
    		return JSON.parse(localStorage.getItem('authUser'));
    	}else{
    		return null;
    	}
    }

    /**
	 * 認証済みか未認証かを返します。
	 */
    isAuth(){
    	if(this._expirationTime(localStorage.getItem('createTime'))){
    		return true;
    	}else{
    		return false;
    	}
    }

    /**
	 * 社員情報の一覧を取得する。
	 */
    getEmpioyeeList(){
    	return this._sendData('get',this.baseUrl + '/api/employee',null,localStorage.getItem('token'));
    }

    /**
	 * 指定のIDの社員情報を取得する
	 */
    getEmpioyee(id){
    	return this._sendData('get',this.baseUrl + '/api/employee/'+id,null,localStorage.getItem('token'));
    }


    /**
	 * トークンが有効か確認する。
	 */
    _expirationTime(time){

    	if(undefined == time || null == time){
    		return false;
    	}

    	var ms = new Date().getTime() - Number(time);
    	var days = Math.floor(ms / (1000*60*60*24));

    	if(0 == days){
    		return true;
    	}else{
    		return false;
    	}
    }


    /**
	 * 通信を実施します。 privateメソッド
	 */
    _sendData(type,url,data,token){
    	return new Promise(function(resolve, reject){

    		var headers={'Content-Type':'application/json'};
    		if(token != undefined && token != null){
    		 headers.Authorization = "token "+token;
    		}

    		var jsonData = null;
    		if(data != undefined && data != null){
    			jsonData = JSON.stringify(data);
    		}

    	    $.ajax({
    	    	type: type,
    	        url: url,
    	        headers:headers,
    	        dataType: 'json',
    	        contentType: 'application/json',
    	       // xhrFields: { withCredentials: true },
    	        data: jsonData,
    	        success: function(data) {
    	        	resolve(data);
    	        },
    	        error: function(error) {
    	        	reject(error.responseJSON);
    	        }
    	       });
    	});
    }
}