/**
 *
 */
package co.jp.tcs.core.app.authorization;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import co.jp.tcs.core.domain.service.AuthorizationService;
import co.jp.tcs.core.exception.UnauthorixedException;

/**
 * 認証実施クラス
 * @author user
 * @see https://qiita.com/dmnlk/items/cce551ce18973f013b36
 *
 */
public class ApiAuthorizationInterceptor implements HandlerInterceptor {

	@Autowired
	private AuthorizationService authService;

	/**
	 * コントラーが呼ばれる前に実行するメソッド
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception, UnauthorixedException {

		// クロスドメインを許可する。
		accessControlAllow(response);

		//静的リソースの場合は認証不要
		if (handler instanceof ResourceHttpRequestHandler) {
			return true;
		}

		// @NonAuthがついてるメソッドは認証不要
		if (isNonAuth(handler)) {
			return true;
		}

		// 認証を実施する
		auth(request);

		return true;
	}

	/**
	 * メソッドにNonAuthのアノテーションが付与されているか確認する。
	 * @param handler
	 * @return
	 */
	private boolean isNonAuth(Object handler) {
		if (handler instanceof HandlerMethod) {
			HandlerMethod hm = (HandlerMethod) handler;
			Method method = hm.getMethod();
			NonAuth annotation = AnnotationUtils.findAnnotation(method, NonAuth.class);
			if (annotation != null) {
				return true;
			}else {
				return false;
			}
		}
		return true;
	}

	/**
	 * 認証を実施する
	 * @param request
	 * @throws UnauthorixedException
	 */
	private void auth(HttpServletRequest request) throws UnauthorixedException {
		String token = request.getHeader("Authorization");
		if (StringUtils.isEmpty(token)) {
			throw new UnauthorixedException("認証されていません");
		}

		authService.isAuth(token.replaceFirst("token", "").trim());
	}

	/**
	 * クロスドメインを許可する。
	 * @param response
	 */
	private void accessControlAllow(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS");
		response.setHeader("Access-Control-Allow-Headers", "*");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		//response.setHeader("Access-Control-Max-Age", 180);
	}
}
