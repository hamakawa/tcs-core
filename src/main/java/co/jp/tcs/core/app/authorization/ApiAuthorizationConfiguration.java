package co.jp.tcs.core.app.authorization;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.MappedInterceptor;

/**
 * 認証の設定
 * @author user
 *
 */
@Configuration
public class ApiAuthorizationConfiguration {

	@Bean
	public ApiAuthorizationInterceptor sampleInterceptor() {
		return new ApiAuthorizationInterceptor();
	}

	@Bean
	public MappedInterceptor interceptor() {
		return new MappedInterceptor(new String[] { "/**" }, sampleInterceptor());
	}
}
