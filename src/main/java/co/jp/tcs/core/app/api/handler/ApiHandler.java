package co.jp.tcs.core.app.api.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.annotation.JsonProperty;

import co.jp.tcs.core.exception.UnauthorixedException;
import co.jp.tcs.core.exception.ValidateException;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Restful用の例外処理や各コントロールの前処理を実施する。
 * @author hamakawa
 * @see https://qiita.com/shotana/items/f3627e45feb912946c7c
 * @see https://terasolunaorg.github.io/guideline/1.0.1.RELEASE/ja/ArchitectureInDetail/REST.html#resthowtouseexceptionhandlingforvalidationerror
 */
@RestControllerAdvice
public class ApiHandler extends ResponseEntityExceptionHandler {

	/**
	 * 認証トークンを取得します
	 * @param token
	 * @return
	 */
	@ModelAttribute("token")
	public String authorizationToken(@RequestHeader("authorization") Optional<String> token) {
		return token.orElse("").replaceFirst("token", "").trim();
	}

	/**
	 * 入力チェックの例外を処理する(@Validatedで発生した例外を処理する)
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		List<FieldError> errorList = ex.getBindingResult().getFieldErrors();
		Map<String, String> erros = new HashMap<String, String>();
		for (FieldError fieldError : errorList) {
			erros.put(fieldError.getField(), fieldError.getDefaultMessage());
		}

		return super.handleExceptionInternal(ex, new ValidationErrorResponse(HttpStatus.BAD_REQUEST.value(), erros),
				headers, status,
				request);
	}

	/**
	 * 入力チェックの例外を処理する(独自の入力チェッククラス)
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(ValidateException.class)
	public ResponseEntity<Object> validateException(ValidateException ex, WebRequest request) {
		return super.handleExceptionInternal(ex,
				new ValidationErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getErrors()),
				null, HttpStatus.BAD_REQUEST,
				request);
	}

	/**
	 * 認証エラーの例外を処理する
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(UnauthorixedException.class)
	public ResponseEntity<Object> unauthorixedException(UnauthorixedException ex, WebRequest request) {

		return super.handleExceptionInternal(ex,
				new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), ex.getMessage()), null,
				HttpStatus.UNAUTHORIZED, request);
	}

	/**
	 * ハンドリングされていない全ての例外をキャッチする。
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> allException(Exception ex, WebRequest request) {

		return super.handleExceptionInternal(ex,
				new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.toString()), null,
				HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	/**
	 * エラーレスポンスクラス
	 * @author user
	 *
	 */
	@AllArgsConstructor
	@Data
	private class ErrorResponse {
		@JsonProperty("code")
		private int code;
		@JsonProperty("message")
		private String message;
	}

	/**
	 * バリデーションエラークラス
	 * @author user
	 *
	 */
	@AllArgsConstructor
	@Data
	private class ValidationErrorResponse {
		@JsonProperty("code")
		private int code;
		@JsonProperty("errors")
		private Map<String, String> errors;
	}
}
