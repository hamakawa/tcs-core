/**
 *
 */
package co.jp.tcs.core.app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.jp.tcs.core.domain.entity.Employee;
import co.jp.tcs.core.domain.entity.Employee.IgnorePassword;
import co.jp.tcs.core.domain.model.AuthUser;
import co.jp.tcs.core.domain.service.AuthorizationService;
import co.jp.tcs.core.domain.service.EmployeeService;
import co.jp.tcs.core.exception.UnauthorixedException;
import co.jp.tcs.core.exception.ValidateException;

/**
 * 社員情報API
 * @author user
 *
 */
@Scope("prototype")
@RestController
@RequestMapping(value = "/tcs/core/api/employee", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class EmployeeApi {

	@Autowired
	private AuthorizationService authService;
	@Autowired
	private EmployeeService employeeService;

	/**
	 * 社員情報の一覧(全件)を取得する。
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public JsonNode getEmployeeList() {

		return new ObjectMapper().addMixIn(Employee.class, IgnorePassword.class)
				.valueToTree(employeeService.getEmployeeAll());
	}

	/**
	 * idに一致する社員情報を取得する。
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public JsonNode getEmployee(@PathVariable Integer id) {

		return new ObjectMapper().addMixIn(Employee.class, IgnorePassword.class)
				.valueToTree(employeeService.getEmployee(id));
	}

	/**
	 * 社員情報を新規に登録する
	 * @param employee 社員情報
	 * @return 作成された社員情報
	 * @throws UnauthorixedException
	 * @throws ValidateException
	 */
	@RequestMapping(method = RequestMethod.POST)
	public Employee registEmployee(@ModelAttribute("token") String token,
			@RequestBody @Validated Employee employee)
			throws UnauthorixedException, ValidateException {

		// 認証済みユーザーを取得する。
		AuthUser user = authService.getAuthUser(token);
		// 社員情報を登録する。
		return employeeService.registEmployee(employee, user.getName());
	}

	/**
	 * idに一致する社員情報を更新する
	 * @param id 社員ID
	 * @param employee 社員情報
	 * @return 更新された社員情報
	 * @throws UnauthorixedException
	 * @throws ValidateException
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public Employee updateEmployee(@ModelAttribute("token") String token, @PathVariable Integer id,
			@RequestBody @Validated Employee employee) throws UnauthorixedException, ValidateException {

		// 認証済みユーザーを取得する。
		AuthUser user = authService.getAuthUser(token);
		// 社員情報を更新する。
		return employeeService.updateEmployee(id, employee, user.getName());
	}

	/**
	 * idに一致する社員情報を削除する。
	 * @return
	 * @throws UnauthorixedException
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteEmployee(@ModelAttribute("token") String token, @PathVariable Integer id)
			throws UnauthorixedException {
		// 認証済みユーザーを取得する。
		AuthUser user = authService.getAuthUser(token);
		employeeService.deleteEmployee(id, user.getName());
	}

}
