/**
 *
 */
package co.jp.tcs.core.app.api;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.jp.tcs.core.app.authorization.NonAuth;
import co.jp.tcs.core.domain.entity.Employee;
import co.jp.tcs.core.domain.entity.Employee.IgnorePassword;
import co.jp.tcs.core.domain.model.AuthUser;
import co.jp.tcs.core.domain.service.AuthorizationService;
import co.jp.tcs.core.exception.UnauthorixedException;
import lombok.Data;

/**
 * 認証API
 * @see https://qiita.com/okoi/items/3bb5ae26ad559e4f39a0
 * @author user
 *
 */
@Scope("prototype")
@RestController
@RequestMapping(value = "/tcs/core/api/auth", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AuthorizationApi {

	@Autowired
	private AuthorizationService authService;

	/**
	 * 認証を実施します。
	 * 認証OKの場合、認証トークンと職員の情報を返す。
	 * 入力エラーの場合 http status 400
	 * 認証NGの場合 http status 401
	 * 入力チェックエラー http stats 402
	 * @param auth
	 * @return
	 * @throws UnauthorixedException
	 */
	@NonAuth
	@RequestMapping(method = RequestMethod.POST)
	public JsonNode auth(@RequestBody @Validated AuthParam auth)
			throws Exception {

		return new ObjectMapper().addMixIn(Employee.class, IgnorePassword.class)
				.valueToTree(authService.auth(auth.getAccount(), auth.getPassword()));
	}

	/**
	 * 認証済みのユーザーを取得する。
	 * 認証OK：http stats 200
	 * 認証NG：http stats 401
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public JsonNode getAuthUser(@ModelAttribute("token") String token) throws Exception {

		return new ObjectMapper().addMixIn(Employee.class, IgnorePassword.class)
				.valueToTree(authService.getAuthUser(token));
	}

	/**
	 * 認証が完了しているかチェックする。
	 * 認証OK：http stats 200
	 * 認証NG：http stats 401
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/check")
	public void isAuth(@ModelAttribute("token") String token) throws Exception {
		authService.isAuth(token);
	}

	/**
	 * 認証をキャンセルする。
	 * ログアウト
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/delete")
	public void delete(@ModelAttribute("token") String token) {
		authService.authCancel(token);
	}

	/**
	 * パスワードを変更する。
	 * @param token
	 * @param auth
	 * @throws UnauthorixedException
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public void passwordChange(@ModelAttribute("token") String token,
			@RequestBody @Validated PasswordChangeParam passwordChangeParam)
			throws UnauthorixedException {
		// 認証済みユーザーを取得する。
		AuthUser user = authService.getAuthUser(token);
		authService.passwordChange(passwordChangeParam.getAccount(), passwordChangeParam.getPassword(),
				passwordChangeParam.getNewPassword(), user.getName());
	}

	/**
	 * 認証パラメータークラス
	 * @author user
	 *
	 */
	@Data
	private static class AuthParam {
		@NotNull(message = "アカウントは必須です")
		private String account;
		@NotNull(message = "パスワードは必須です")
		private String password;
	}

	/**
	 * パスワードを変更する。
	 * @author user
	 *
	 */
	@Data
	private static class PasswordChangeParam extends AuthParam {
		@NotNull(message = "新しいパスワードは必須です")
		private String newPassword;
	}
}
