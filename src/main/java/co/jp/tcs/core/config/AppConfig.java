/**
 *
 */
package co.jp.tcs.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import co.jp.tcs.core.domain.model.AuthUser;

/**
 * アプリケーションで共通で使用する設定を行うクラス
 * @author user
 *
 */
@Configuration
public class AppConfig {

	/**
	 * アクセスログを出力する。
	 * @return
	 */
	@Bean
	public CommonsRequestLoggingFilter requestLoggingFilter() {
		CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
		filter.setIncludeClientInfo(true);
		filter.setIncludeQueryString(true);
		filter.setIncludeHeaders(true);
		filter.setIncludePayload(true);
		filter.setMaxPayloadLength(1024);
		return filter;
	}

	/**
	 * ハッシュ化
	 * @return
	 */
	@Bean
	public PasswordEncoder bCrypt() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * redisでjsonを扱う
	 * @param connectionFactory
	 * @return
	 */
	@Bean
	public RedisTemplate<String, AuthUser> jsonRedisTemplate(RedisConnectionFactory connectionFactory) {
		RedisTemplate<String, AuthUser> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(connectionFactory);
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(AuthUser.class));
		redisTemplate.setHashKeySerializer(redisTemplate.getKeySerializer());
		redisTemplate.setHashValueSerializer(redisTemplate.getValueSerializer());
		return redisTemplate;
	}

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

}