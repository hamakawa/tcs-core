/**
 *
 */
package co.jp.tcs.core.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * 社員エンティティクラス
 * @author user
 *
 */
@Entity
@Table(name = "employees")
@Data
public class Employee extends CommonEntity implements Serializable {

	/** 社員ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	/** 社員番号 */
	@Column(name = "employee_no")
	@Pattern(regexp="[0-9]+",message = "社員番号は数値のみです")
	@NotNull(message = "社員番号は必須です")
	@Size(max = 10, message = "社員番号は10桁以内です")
	private String employeeNo;

	/** 姓 */
	@Column(name = "last_name")
	@NotNull(message = "姓は必須です")
	@Size(max = 255, message = "姓は255文字以内です")
	private String lastName;

	/** 名 */
	@Column(name = "first_name")
	@NotNull(message = "名は必須です")
	@Size(max = 255, message = "名は255文字以内です")
	private String firstName;

	/** 姓カナ */
	@Column(name = "last_name_kana")
	@Size(max = 255, message = "姓カナは255文字以内です")
	private String lastNameKana;

	/** 名カナ */
	@Column(name = "first_name_kana")
	@Size(max = 255, message = "名カナは255文字以内です")
	private String firstNameKana;

	/** 電話番号 */
	@Column(name = "tel")
	@Pattern(regexp="[0-9]+",message = "電話番号は数値のみです")
	@Size(max = 255, message = "電話番号は255桁以内です")
	private String tel;

	/** 携帯番号 */
	@Column(name = "mobile_tel")
	@Pattern(regexp="[0-9]+",message = "携帯番号は数値のみです")
	@Size(max = 255, message = "携帯番号は255桁以内です")
	private String mobileTel;

	/** メール */
	@Column(name = "mail")
	@Email(message = "メールの形式が不正です")
	@Size(max = 255, message = "メールは255文字以内です")
	private String mail;

	/** アカウント */
	@Column(name = "account")
	@Pattern(regexp="[0-9a-zA-Z]+",message = "アカウントは英数字のみです")
	@NotNull(message = "アカウントは必須です")
	@Size(max = 255, message = "アカウントは255文字以内です")
	private String account;

	/** パスワード */
	@Column(name = "password")
	@Pattern(regexp="[0-9a-zA-Z]+",message = "パスワードは英数字のみです")
	@Size(max = 255, message = "パスワードは255文字以内です")
	private String password;

	/**
	 * 名前を取得する。
	 * @return
	 */
	@JsonIgnore
	public String getName() {
		return firstName + " " + lastName;
	}

	/**
	 * 名前カナを取得する。
	 * @return
	 */
	@JsonIgnore
	public String getNameKana() {
		return firstNameKana + " " + lastNameKana;
	}

	/**
	 * json出力除外項目
	 * @author user
	 *
	 */
	public interface IgnorePassword {
		@JsonIgnore
		String getPassword();
	}
}
