/**
 *
 */
package co.jp.tcs.core.domain.service.impl;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.jp.tcs.core.domain.entity.Employee;
import co.jp.tcs.core.domain.model.AuthUser;
import co.jp.tcs.core.domain.repository.EmployeeRepository;
import co.jp.tcs.core.domain.service.AuthorizationService;
import co.jp.tcs.core.exception.UnauthorixedException;

/**
 * 認証を実施するサービス
 * @author user
 *
 */
@Profile({"dev", "prod"})
@Scope("prototype")
@Service
public class AuthorizationServiceImpl implements AuthorizationService {

	@Autowired
	private RedisTemplate<String, AuthUser> redis;
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private PasswordEncoder bCrypt;

	@Override
	public AuthUser auth(String account, String password) throws UnauthorixedException {
		try {
			// アカウントとパスワードに一致した社員情報を取得する
			Employee employee = getEmployeeByAccount(account, password);

			// 認証済のログインユーザーを生成する
			AuthUser user = AuthUser.create(UUID.randomUUID().toString(), employee);

			// メモリキャッシュに保存する(保存期間1日)
			redis.opsForValue().set(user.getToken(), user);
			redis.expire(user.getToken(), 1, TimeUnit.DAYS);
			return user;
		} catch (UnauthorixedException e) {
			throw new UnauthorixedException("認証に失敗しました");
		}
	}

	@Override
	public void isAuth(String token) throws UnauthorixedException {
		getAuthUser(token);
	}

	public AuthUser getAuthUser(String token) throws UnauthorixedException {
		AuthUser authUser = redis.opsForValue().get(token);
		if (null == redis.opsForValue().get(token)) {
			throw new UnauthorixedException("認証されていません");
		}
		return authUser;
	}

	@Override
	public void authCancel(String token) {
		redis.delete(token);
	}

	@Override
	@Transactional
	public void passwordChange(String account, String oldPassword, String newPassword, String updateUserName)
			throws UnauthorixedException {

		try {
			Employee employee = getEmployeeByAccount(account, oldPassword);
			employee.setPassword(bCrypt.encode(newPassword));
			employee.setUpdatedUser(updateUserName);
			employeeRepository.save(employee);
		} catch (UnauthorixedException e) {
			throw new UnauthorixedException("アカウントまたはパスワードが間違っています");
		}
	}

	/**
	 * アカウント、パスワードに一致する社員情報を取得する
	 * @param account
	 * @param password
	 * @return
	 * @throws UnauthorixedException
	 */
	private Employee getEmployeeByAccount(String account, String password) throws UnauthorixedException {

		System.out.println(bCrypt.encode(password));


		Employee employee = employeeRepository.findByIsDeletedFalseAndAccount(account);
		if (employee != null && bCrypt.matches(password, employee.getPassword())) {
			return employee;
		} else {
			throw new UnauthorixedException();
		}
	}
}
