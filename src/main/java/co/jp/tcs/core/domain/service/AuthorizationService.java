/**
 *
 */
package co.jp.tcs.core.domain.service;

import co.jp.tcs.core.domain.model.AuthUser;
import co.jp.tcs.core.exception.UnauthorixedException;;

/**
 * 認証を実施するサービス
 * @author user
 *
 */
public interface AuthorizationService {
	/**
	 * 認証を実施する
	 * @param account
	 * @param password
	 * @return 認証済み利用者(社員)を返す。
	 * @throws UnauthorixedException
	 */
	public AuthUser auth(String account, String password) throws UnauthorixedException;

	/**
	 * 認証済みユーザーを取得する。
	 * @param token
	 * @throws UnauthorixedException
	 */
	public AuthUser getAuthUser(String token) throws UnauthorixedException;

	/**
	 * 認証が完了しているかチェックする
	 * @param token
	 * @throws UnauthorixedException
	 */
	public void isAuth(String token) throws UnauthorixedException;

	/**
	 * 認証をキャンセルする
	 * @param token
	 */
	public void authCancel(String token);

	/**
	 * パスワードを変更する
	 * @param account
	 * @param oldPassword
	 * @param newPassword
	 * @param updateUserName
	 * @throws UnauthorixedException
	 */
	public void passwordChange(String account, String oldPassword, String newPassword, String updateUserName)
			throws UnauthorixedException;
}
