/**
 *
 */
package co.jp.tcs.core.domain.model;

import java.io.Serializable;

import org.modelmapper.ModelMapper;

import co.jp.tcs.core.domain.entity.Employee;
import lombok.Data;

/**
 * 認証済み利用者(社員)を表現する
 * @author user
 *
 */
@Data
public class AuthUser extends Employee implements Serializable {

	/** 認証済みトークン */
	private String token;

	/**
	 * 社員エンティティから認証済み利用者(社員)を生成する。
	 * @param employee
	 * @return
	 */
	public static AuthUser create(String token, Employee employee) {

		AuthUser authUser = new ModelMapper().map(employee, AuthUser.class);
		authUser.setToken(token);
		return authUser;
	}

}
