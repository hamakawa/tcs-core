/**
 *
 */
package co.jp.tcs.core.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * エンティティクラス共通項目クラス
 * @author user
 *
 */
@MappedSuperclass
@Data
public class CommonEntity implements Serializable {

	/** 作成日時 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at", updatable = false)
	@JsonIgnore
	private Date createdAt = new Date();

	/** 作成者 */
	@Column(name = "created_user", updatable = false)
	@JsonIgnore
	private String createdUser;

	/** 更新日時 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	@JsonIgnore
	private Date updatedAt = new Date();

	/** 更新者 */
	@Column(name = "updated_user")
	@JsonIgnore
	private String updatedUser;

	/** 削除フラグ */
	@Column(name = "is_deleted")
	@JsonIgnore
	private Boolean isDeleted = false;

	/**
	 * 新規登録時の作成者、作成時刻を生成を設定する
	 * @param createdUser
	 */
	@JsonIgnore
	public void setCreated(String createdUserName) {
		this.createdUser = createdUserName;
		this.updatedUser = createdUserName;
	}
}
