package co.jp.tcs.core.domain.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import co.jp.tcs.core.domain.entity.Employee;
import co.jp.tcs.core.domain.repository.EmployeeRepository;
import co.jp.tcs.core.domain.service.EmployeeService;
import co.jp.tcs.core.exception.ValidateException;

/**
 * 社員に関するビジネスロジッククラス
 * @author user
 *
 */
@Scope("prototype")
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private PasswordEncoder bCrypt;

	@Override
	@Transactional(readOnly = true)
	public List<Employee> getEmployeeAll() {
		return employeeRepository.findByIsDeletedFalseOrderByEmployeeNoAsc();
	}

	@Override
	@Transactional(readOnly = true)
	public Employee getEmployee(Integer id) {
		return employeeRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Employee registEmployee(Employee employee, String registUserName) throws ValidateException {

		if (getEmployee(employee.getEmployeeNo(), employee.getAccount()) != null) {
			validateException();
		}

		String password = employee.getPassword();
		if (StringUtils.isEmpty(password)) {
			passwordException();
		}

		// パスワードをハッシュ化する
		employee.setPassword(bCrypt.encode(password));

		employee.setCreated(registUserName);
		employeeRepository.save(employee);
		return employee;
	}

	@Override
	@Transactional
	public Employee updateEmployee(Integer id, Employee employee, String updateUserName) throws ValidateException {

		Employee oldEmployee = getEmployee(employee.getEmployeeNo(), employee.getAccount());
		if (!oldEmployee.getId().equals(id)) {
			validateException();
		}

		employee.setId(id);
		employee.setPassword(oldEmployee.getPassword());
		employee.setUpdatedUser(updateUserName);
		employeeRepository.save(employee);
		return employee;
	}

	@Override
	@Transactional
	public void deleteEmployee(Integer id, String updateUserName) {
		Employee employee = employeeRepository.findById(id).orElse(null);
		employee.setIsDeleted(true);
		employee.setUpdatedUser(updateUserName);
		employeeRepository.save(employee);
	}

	/**
	 * 社員番号またはアカウントに一致する社員情報を取得する
	 */
	private Employee getEmployee(String employeeNo, String account) {
		return employeeRepository.findByIsDeletedFalseAndEmployeeNoOrAccount(employeeNo, account);
	}

	/**
	 * 社員番号またはアカウントは既に登録されている場合、このメソッドを実行する。
	 */
	private void validateException() throws ValidateException {
		Map<String, String> erros = new HashMap<String, String>();
		erros.put("unique_employee_error", "社員番号またはアカウントは既に登録されています");
		throw new ValidateException(erros);
	}

	private void passwordException() throws ValidateException {
		Map<String, String> erros = new HashMap<String, String>();
		erros.put("password", "パスワードが設定されていません");
		throw new ValidateException(erros);
	}
}