/**
 *
 */
package co.jp.tcs.core.domain.service;

import java.util.List;

import co.jp.tcs.core.domain.entity.Employee;
import co.jp.tcs.core.exception.ValidateException;

/**
 * 社員に関するビジネスロジックを処理するインターフェース
 * @author user
 *
 */
public interface EmployeeService {

	/**
	 * 社員情報の一覧(全件)を取得する。
	 * @return
	 */
	public List<Employee> getEmployeeAll();

	/**
	 * idに一致する社員情報を取得する。
	 * @param id
	 * @return
	 */
	public Employee getEmployee(Integer id);

	/**
	 * 社員情報を新規に登録する
	 * @param employee 社員情報
	 * @param registUserId 登録したユーザー名
	 * @return
	 */
	public Employee registEmployee(Employee employee, String registUserName)throws ValidateException;

	/**
	 * idに一致する社員情報を更新する
	 * @param id 更新する社員情報のID
	 * @param employee 社員情報
	 * @param updateUserId 更新したユーザー名
	 * @return
	 */
	public Employee updateEmployee(Integer id, Employee employee, String updateUserName)throws ValidateException;

	/**
	 * idに一致する社員情報を削除する。
	 * @param id
	 * @param updateUserId 更新したユーザー名
	 */
	public void deleteEmployee(Integer id, String updateUserName);
}
