package co.jp.tcs.core.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.jp.tcs.core.domain.entity.Employee;

/**
 * 社員情報リポジトリ
 *
 */
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	/**
	 * アカウントに一致する社員情報を取得する
	 * @param account
	 * @return
	 */
	public Employee findByIsDeletedFalseAndAccount(String account);

	/**
	 * 削除済みフラグが立っていない全ての社員情報を取得する
	 * @return
	 */
	public List<Employee> findByIsDeletedFalseOrderByEmployeeNoAsc();

	/**
	 * 社員番号またはアカウントに紐づく社員情報を取得する。
	 * @param employeeNo
	 * @param account
	 * @return
	 */
	public Employee findByIsDeletedFalseAndEmployeeNoOrAccount(String employeeNo, String account);
}