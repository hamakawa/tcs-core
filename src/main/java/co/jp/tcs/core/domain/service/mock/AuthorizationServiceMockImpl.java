/**
 *
 */
package co.jp.tcs.core.domain.service.mock;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.jp.tcs.core.domain.model.AuthUser;
import co.jp.tcs.core.domain.service.AuthorizationService;
import co.jp.tcs.core.exception.UnauthorixedException;

/**
 * 認証を実施するサービスのモック
 * @author user
 *
 */

@Profile("dev2")
@Scope("prototype")
@Service
public class AuthorizationServiceMockImpl implements AuthorizationService {

	@Override
	public AuthUser auth(String account, String password) throws UnauthorixedException {
		return createAuthUser();
	}

	@Override
	public void isAuth(String token) throws UnauthorixedException {

	}

	public AuthUser getAuthUser(String token) throws UnauthorixedException {
		return createAuthUser();
	}

	@Override
	public void authCancel(String token) {

	}

	private AuthUser createAuthUser() {

		AuthUser user = new AuthUser();
		user.setToken("1234567890");
		user.setId(-1);
		user.setLastName("てすと");
		user.setFirstName("てすお");
		user.setLastNameKana("テスト");
		user.setFirstNameKana("テスオ");
		user.setAccount("test");

		return user;
	}

	@Override
	@Transactional
	public void passwordChange(String account, String oldPassword, String newPassword, String updateUserName)
			throws UnauthorixedException {

	}

}
