package co.jp.tcs.core;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TcsCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(TcsCoreApplication.class, args);
	}
}
