package co.jp.tcs.core.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 認証エラーの例外
 * @author user
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UnauthorixedException extends Exception {

	private String message;

}
