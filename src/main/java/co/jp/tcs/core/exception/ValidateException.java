package co.jp.tcs.core.exception;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 入力チェックエラー
 * @author user
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ValidateException extends Exception {

	private Map<String, String> errors;
}
